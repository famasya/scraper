import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """ Default configurations """

    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://scraper:scraper@localhost/scraper"
    JOB_SECRET_STRING = "Jq5BDqNmSctPsz5glc3z-6apvC6XkRrPBJMRNg5Sj"
    BASE_URL = "http://31.220.60.94:5000/api/v1"
    # SQLALCHEMY_DATABASE_URI = "sqlite:///" + \
    #     os.path.join(basedir, "bucketlist.db")


class DevelopmentConfig(Config):
    """ Development configurations """

    DEBUG = True
    TESTING = True
    # SQLALCHEMY_DATABASE_URI = "sqlite:///" + \
    #     os.path.join(basedir, "bucketlist.db")
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://scraper:scraper@localhost/scraper"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "p9Bv<3Eid9%$i01"


class TestingConfig(Config):
    """ Test configurations """

    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + \
        os.path.join(basedir, "test.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "p9Bv<3Eid9%$i01"


class ProductionConfig(Config):
    """ Production configurations """

    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://scraper:scraper@localhost/scraper"
    # SQLALCHEMY_DATABASE_URI = "sqlite:///models/bucketlist.db"

app_config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig
}
